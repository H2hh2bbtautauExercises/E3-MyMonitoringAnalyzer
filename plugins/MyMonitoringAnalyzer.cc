#include <vector>

#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/one/EDAnalyzer.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "CommonTools/UtilAlgos/interface/TFileService.h"

#include "DataFormats/PatCandidates/interface/Muon.h"
#include "TH1.h"


class MyMonitoringAnalyzer : public edm::one::EDAnalyzer<edm::one::SharedResources>  {
public:
  explicit MyMonitoringAnalyzer(const edm::ParameterSet&);
  ~MyMonitoringAnalyzer();
  
  static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);
  
  
private:
  virtual void beginJob() override;
  virtual void analyze(const edm::Event&, const edm::EventSetup&) override;
  virtual void endJob() override;
  
  edm::EDGetTokenT<pat::MuonCollection> muonsToken_;

  TH1F* hist_yield;
  TH1F* hist_mu_pt;
  TH1F* hist_mu_eta;
  TH1F* hist_mu_iso;
  TH1F* hist_mu_mult;
};

//
// constructors and destructor
//
MyMonitoringAnalyzer::MyMonitoringAnalyzer(const edm::ParameterSet& iConfig)
{
   usesResource("TFileService");
   muonsToken_ = consumes<pat::MuonCollection>(iConfig.getParameter<edm::InputTag>("muons"));
}


MyMonitoringAnalyzer::~MyMonitoringAnalyzer()
{
}


// ------------ method called for each event  ------------
void
MyMonitoringAnalyzer::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup)
{
  edm::Handle<pat::MuonCollection> muons;
  iEvent.getByToken(muonsToken_,muons);

  hist_yield->Fill(0.5);
  hist_mu_mult->Fill(muons->size());

  for(std::vector<pat::Muon>::const_iterator mu=muons->begin(); mu!=muons->end(); ++mu){
    hist_mu_pt->Fill(mu->pt());
    hist_mu_eta->Fill(mu->eta());
    hist_mu_iso->Fill(mu->userIsolation(pat::IsolationKeys(7)));
  }
}


// ------------ method called once each job just before starting event loop  ------------
void 
MyMonitoringAnalyzer::beginJob()
{
  //register to the TFileService
  edm::Service<TFileService> fs;

  // book histograms:
  hist_yield   = fs->make<TH1F>("yield"    , "event yield"                , 1  , 0., 1.);
  hist_mu_mult = fs->make<TH1F>("mumult"   , "muon multiplicity;N;events" , 10 , 0., 10.);
  hist_mu_pt   = fs->make<TH1F>("mupt"     , "muon pt;pt[GeV];muons"      , 50 , 0., 100.);
  hist_mu_eta  = fs->make<TH1F>("mueta"    , "muon eta;eta;muons"         , 40 , -3, 3);
  hist_mu_iso  = fs->make<TH1F>("muiso"    , "muon iso;iso[GeV];muons"    , 80 , 0., 4.);
}

// ------------ method called once each job just after ending the event loop  ------------
void 
MyMonitoringAnalyzer::endJob() 
{
}

// ------------ method fills 'descriptions' with the allowed parameters for the module  ------------
void
MyMonitoringAnalyzer::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
  edm::ParameterSetDescription desc;
  desc.add<edm::InputTag>("muons")->setComment("input muon collection");
  descriptions.addDefault(desc);
}

//define this as a plug-in
DEFINE_FWK_MODULE(MyMonitoringAnalyzer);
