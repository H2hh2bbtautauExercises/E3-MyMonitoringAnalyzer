import FWCore.ParameterSet.Config as cms

process = cms.Process("EXERCISE")
process.load("FWCore.MessageService.MessageLogger_cfi")
process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(-1) )
process.options  = cms.untracked.PSet( wantSummary = cms.untracked.bool(True))

process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(
        'file:/pnfs/desy.de/cms/tier2/store/mc/RunIISpring16MiniAODv2/GluGluToRadionToHHTo2B2Tau_M-300_narrow_13TeV-madgraph/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14_ext1-v1/80000/02EE1330-5D3B-E611-B5AD-001CC4A7C0A4.root'
    )
)

process.myMuonCollectionWithIsolation = cms.EDProducer('MyMuonIsolationProducer',
    muons = cms.InputTag('slimmedMuons')
)

process.selectedMuons1 = cms.EDFilter("MyPATMuonCloneSelectorFilter",
                                     src = cms.InputTag('myMuonCollectionWithIsolation'),
                                     vertices = cms.InputTag('offlineSlimmedPrimaryVertices'),
                                     ptmin = cms.double(0),
                                     etamax = cms.double(10),   
                                     vtxdxymax = cms.double(0.045),
                                     vtxdzmax = cms.double(0.2),
                                     takeMuonID = cms.string("medium"),
                                     iso = cms.double(100),
                                     filter = cms.bool(True)
                                     )

process.selectedMuons2 = process.selectedMuons1.clone()
process.selectedMuons2.src=cms.InputTag('selectedMuons1')
process.selectedMuons2.etamax=cms.double(2.1)

process.selectedMuons3 = process.selectedMuons2.clone()
process.selectedMuons3.src=cms.InputTag('selectedMuons2')
process.selectedMuons3.ptmin=cms.double(20)

process.selectedMuons4 = process.selectedMuons3.clone()
process.selectedMuons4.src=cms.InputTag('selectedMuons3')
process.selectedMuons4.iso=cms.double(0.15)


process.mon_all = cms.EDAnalyzer("MyMonitoringAnalyzer",
                              muons = cms.InputTag('myMuonCollectionWithIsolation')
                             )

process.mon_ID_vtx = process.mon_all.clone()
process.mon_ID_vtx.muons=cms.InputTag('selectedMuons1')

process.mon_eta = process.mon_ID_vtx.clone()
process.mon_eta.muons=cms.InputTag('selectedMuons2')

process.mon_pt = process.mon_eta.clone()
process.mon_pt.muons=cms.InputTag('selectedMuons3')

process.mon_iso = process.mon_pt.clone()
process.mon_iso.muons=cms.InputTag('selectedMuons4')



process.out = cms.OutputModule("PoolOutputModule",
    fileName = cms.untracked.string('mySelectedMuonOutputFile.root'),
    outputCommands = cms.untracked.vstring(
        'drop *',
        'keep *_slimmedMuons_*_*',
        'keep *_*_*_EXERCISE'
        ),
    SelectEvents = cms.untracked.PSet(SelectEvents = cms.vstring("p"))
)
  
process.p = cms.Path(process.myMuonCollectionWithIsolation
                     *process.mon_all
                     *process.selectedMuons1
                     *process.mon_ID_vtx
                     *process.selectedMuons2
                     *process.mon_eta
                     *process.selectedMuons3
                     *process.mon_pt
                     *process.selectedMuons4
                     *process.mon_iso
                     )

process.e = cms.EndPath(process.out)

process.TFileService = cms.Service("TFileService",
                                   fileName = cms.string("monitoring.root")
                                   )
